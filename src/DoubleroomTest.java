import org.junit.Test;

import static org.junit.Assert.*;

public class DoubleroomTest {
    @Test
    public void doubleRoomTestEquals(){
        var obj = new Doubleroom("Sampath","9573385110","Male","Diyyala","6302021604","Male");
        assertEquals("9573385110",obj.contact);
    }
    @Test
    public void doubleRoomTestTrue(){
        var obj = new Doubleroom("Sampath","9573385110","Male","Diyyala","6302021604","Male");
        assertTrue("9573385110"==obj.contact);
    }
    @Test
    public void doubleRoomTestSame(){
        var obj = new Doubleroom("Sampath","9573385110","Male","Diyyala","6302021604","Male");
        assertSame("9573385110",obj.contact);
    }

}