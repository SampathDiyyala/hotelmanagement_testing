import org.junit.Test;

import static org.junit.Assert.*;

public class SingleroomTest {
    @Test
    public void singleRoomTestEquals(){
        var obj = new Singleroom("Sampath","9573385110","Male");
        assertEquals("Sampath",obj.name);
    }
    @Test
    public void singleRoomSame(){
        var obj = new Singleroom("Sampath","9573385110","Male");
        assertSame("Sampath",obj.name);
    }
    @Test
    public void singleRoomTestTrue(){
        var obj = new Singleroom("Sampath","9573385110","Male");
        assertTrue("Sampath"==obj.name);
    }
    @Test
    public void singleRoomTestNotNull(){
        var obj = new Singleroom("Sampath","9573385110","Male");
        assertNotNull(obj.name);
    }

}