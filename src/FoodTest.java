import org.junit.Test;

import static org.junit.Assert.*;

public class FoodTest {
    @Test
    public void foodTestEquals(){
        var food = new Food(1,1);
       assertNotNull(food.price);
    }
    @Test
    public void foodTestTrue(){
        var food = new Food(1,1);
        assertTrue(50==food.price);
    }
}