import org.junit.Test;

import static org.junit.Assert.*;

public class HolderTest {
    @Test
    public void holderTestEquals(){
        var obj = new holder();
        assertEquals(10,obj.luxury_doublerrom.length);
    }
    @Test
    public void holderTestTrue(){
        var obj = new holder();
        assertTrue(10==obj.luxury_singleerrom.length);
    }
    @Test
    public void holderTestNotNull(){
        var obj = new holder();
        assertNotNull(obj.deluxe_doublerrom.length);
    }
    @Test
    public void holderTestSame(){
        var obj = new holder();
        assertSame(20,obj.deluxe_doublerrom.length);
    }

}