import org.junit.Test;

import static org.junit.Assert.*;

public class HotelTest {

    // Feautures maethod test cases

    @Test
    public void featuresTest(){
        var obj = new Hotel();
        String str = ("Number of double beds : 1\nAC : Yes\nFree breakfast : Yes\nCharge per day:4000 ");

        assertEquals(str,obj.features(1));
    }

    // Availability method test cases

    @Test
    public void availabilityTestEquals(){
        var obj = new Hotel();
//        assertEquals("Number of rooms available : "+20,obj.availability(2));
        assertEquals("Number of rooms available : "+19,obj.availability(2));
    }
    @Test
    public void availabilityTestNotNull(){
        var obj = new Hotel();
        assertNotNull("Number of rooms available : "+20,obj.availability(2));
    }
    @Test
    public void availabilityTestNull(){
        var obj = new Hotel();
        var assign =obj.availability(2);
        assign = null;
        assertNull("Number of rooms available : "+20,assign);
    }

    //Custemar Deatails methode test cases

    @Test
    public void custDetailsTestEquals(){
        var obj = new Hotel();
        assertEquals("Diyyala",obj.CustDetails(1,2));
    }
    @Test
    public void custDetailsTestSame(){
        var obj = new Hotel();
        assertSame("Diyyala",obj.CustDetails(1,2));
    }
    @Test
    public void custDetailsTestTrue(){
        var obj = new Hotel();
        assertTrue("Diyyala"==obj.CustDetails(1,2));
    }
    @Test
    public void custDetailsTestNotNull(){
        var obj = new Hotel();
        assertNotNull(obj.CustDetails(1,2));
    }

    //BookRoom method test cases

    @Test
    public void bookRoomtestEquals(){
        var obj = new Hotel();
        String str = "1,2,4,5,6,7,8,9,10,";
//        String str = "1,2,3,4,5,6,7,8,9,10,";
        assertEquals(str,obj.bookroom(1));
    }
    @Test
    public void bookRoomtestSame(){
        var obj = new Hotel();
        assertNotNull(obj.bookroom(2));
    }



}